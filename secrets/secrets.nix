let
  #user1 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL0idNvgGiucWgup/mP78zyC23uFjYq0evcWdjGQUaBH";
  #user2 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILI6jSq53F/3hEmSs+oq9L4TwOo1PrDMAgcA1uo1CCV/";
  romen_bitbucket = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOQ9ojJKkYDewbU34gtx+0LxcWEJTF5/W5wAoYxxaZwY romen@bitbucket_ed25519";
  users = [ romen_bitbucket ];

  #system1 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPJDyIr/FSz1cJdcoW69R+NrWzwGK/+3gJpqD1t8L2zE";
  #system2 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKzxQgondgEYcLpcPdJLrTdNgZ2gznOHCAxMdaceTUT1";
  nixos-runner = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOE2CP2rIzZCCaX5xemmUnD1GEGiFVLnOynHo1yoI/Oa";
  #systems = [ system1 system2 ];
in
{
  #"secret1.age".publicKeys = [ user1 system1 ];
  #"secret2.age".publicKeys = users ++ systems;
  "ci_env.age".publicKeys = [ romen_bitbucket nixos-runner ];
}

